    <h2> Categorie </h2>
            <ul>
                <?php
                    $query = $conn->query("SELECT * FROM categoria");
                    if($query->num_rows){
                        while($row = $query->fetch_assoc()){
                            $id_cat = $row["id"];
                            $nome_cat = $row["nome"];
                            echo("
                            <li>
                            <div class=\"dropdown\">
                                <span class=\"dropdown-click\"><form style=\"display:inline-block;\" action=\"\" method=\"post\"><button type=\"submit\" name=\"filterCat\" style=\"background: white;color: black;font-size: 16px;padding: 0;height: auto;\">$nome_cat</button><input type=\"hidden\" value=\"$id_cat\" name=\"cat\"></form><i class=\"fas fa-chevron-down mobile-drop\"
                                        style=\"float: right;\"></i></span>
                            ");
                            $query_sotcat = $conn->query("SELECT * FROM sottocategoria WHERE id_cat = $id_cat");
                            if($query_sotcat ->num_rows){
                                while ($row = $query_sotcat->fetch_assoc()){
                                    $id_sotcat = $row["id"];
                                    $nome_sotcat = $row["nome"];
                                    echo("
                                    <div class=\"dropdown-content\">
                                    <ul class=\"noborder\">
                                        <li><form action=\"\" method=\"post\"><button type=\"submit\" name=\"filterSotCat\" style=\"background: white;color: black;font-size: 16px;padding: 0;height: auto;\">$nome_sotcat</button><input type=\"hidden\" value=\"$id_sotcat\" name=\"sotcat\"></form></li>
                                    </ul>
                                </div>
                                    ");
                                }
                            }
                            echo("
                            </div>
                            </li>
                            ");

                        }
                    }
                ?>
            </ul>
