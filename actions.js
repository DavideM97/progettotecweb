
        function registrationUser(){
            var nomeregC=document.getElementById("nomeregCli");
            var emailregC=document.getElementById("emailregCli");
            var pswregC=document.getElementById("passwordregCli");
            var confPswregC=document.getElementById("confPasswordregCli");
            var errorBox=document.getElementById("errorMessage2");
            var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
            var goodAlertDiv='<div class="alert alert-success alert-dismissible" role="alert">';
            var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

            emailregC.style.border = "1px solid #ccc";
            pswregC.style.border = "1px solid #ccc";
            confEmailregC.style.border = "1px solid #ccc";
            confPswregC.style.border = "1px solid #ccc";
            nomeregC.style.border = "1px solid #ccc";
            cognomeregC.style.border = "1px solid #ccc";

            if(nomeregC.value==""){
              errorBox.innerHTML=alertDiv+'<strong>Attento</strong> hai dimenticato il nome!'+"</div>";
              nomeregC.focus();
              nomeregC.style.border = "3px solid #990033";
              return false;
            }
            if(emailregC.value==""){
              errorBox.innerHTML=alertDiv+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
              emailregC.focus();
              emailregC.style.border = "3px solid #990033";
              return false;
            }
            if(pswregC.value==""){
              errorBox.innerHTML=alertDiv+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
              pswregC.focus();
              pswregC.style.border = "3px solid #990033";
              return false;
            }
            if(confPswregC.value==""){
              errorBox.innerHTML=alertDiv+'<strong>Attento</strong> hai dimenticato la conferma password!'+"</div>";
              confPswregC.focus();
              confPswregC.style.border = "3px solid #990033";
              return false;
            }
            if(pswregC.value!=confPswregC.value){
              errorBox.innerHTML=alertDiv+'<strong>Attento</strong> il campo conferma password non corrisponde!'+"</div>";
              confPswregC.value="";
              confPswregC.focus();
              confPswregC.style.border = "3px solid #990033";
              return false;
            }

            var reg=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if(!reg.test(emailregC.value)){
              errorBox.innerHTML=alertDiv+'<strong>Attento</strong> il campo mail non è valido!'+"</div>";
              emailregC.focus();
              emailregC.style.border = "3px solid #990033";
              return false;
            }
            return true;
          }

          function validateUser(){
            var emailCli=document.getElementById("emailAccedi");
            var pswCli=document.getElementById("passwordAccedi");
            var errorBox=document.getElementById("errorMessage");
            var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
            var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

            emailCli.style.border = "1px solid #ccc";
            pswCli.style.border = "1px solid #ccc";

            if(emailCli.value==""){
            errorBox.innerHTML=alertDiv+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
            emailCli.focus();
            emailCli.style.border = "3px solid #990033";
            return false;
            }
            if(pswCli.value==""){
            errorBox.innerHTML=alertDiv+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
            pswCli.focus();
            pswCli.style.border = "3px solid #990033";
            return false;
            }
            var reg=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if(!reg.test(emailCli.value)){
            errorBox.innerHTML=alertDiv+'<strong>Attento</strong> il campo mail non è valido!'+"</div>";
            emailCli.focus();
            emailCli.style.border = "3px solid #990033";
            return false;
            }
            return true;
        }



function toggle(id){

    const login_modal = document.getElementById(id);
    if(login_modal.classList.contains('toggled')){
        login_modal.style.opacity = "0";
        login_modal.style.pointerEvents = "none";
        login_modal.classList.remove('toggled');
    }else{
        login_modal.style.opacity = "1";
        login_modal.style.pointerEvents = "all";
        login_modal.classList.add('toggled')
    }

}
function slideIn(id){

    const el = document.getElementById(id);
    el.style.left = "0";

}
function slideOut(id){

    const el = document.getElementById(id);
    el.style.left = "-100vw";

}
