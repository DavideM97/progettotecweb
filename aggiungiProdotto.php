<?php
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST["createProd"])){
  require_once("addNotifica.php");
  $nome = $_POST["nome"];
  $cat = $_POST["cat"];
  $sottocat = $_POST["sotcat"];
  $descrizione = $_POST["descr"];
  $img = $_POST["img"];
  $prezzo = $_POST["prezzo"];
  $conn->set_charset("utf8");

  $query = "INSERT INTO prodotto(nome,descrizione,immagine,prezzo,id_cat,id_sottocat) VALUES ( ?, ?, ?, ?, ?, ?)";

  $st=$conn->stmt_init();
  if($st->prepare($query)){
    $st->bind_param('sssiii',$nome,$descrizione,$img,$prezzo,$cat,$sottocat);
    $st->execute();
  }

  add_notifica("prodotto_creato",$conn);
}
?>
