<header id="header">
        <div class="container flexbox justify-between align-center wrap" id="main-h">
            <h1 class="logo"><a href="index.php" style="color: white;text-decoration: none;">Plantemall</a></h1>
            <div>
                <div class="controls flexbox align-center wrap">
                    <form action="ricerca.php" method="GET">
                        <div class="row">
                            <i class="fas fa-search" style="position:initial"></i>
                            <input type="search" name="q" placeholder="Cerca una piantina...">
                        </div>
                    </form>
                    <div class="icons-mobile" style="display:flex">
                      <div class="icon">
                          <a href="cart.php" style="color:white;"><i class="fas fa-shopping-cart"></i></a>
                      </div>
                      <div class="icon">
                          <a href="ordini.php" style="color:white;"><i class="fas fa-user"></i></a>
                      </div>
                    </div>
                </div>
                <div class="flexbox" style="margin-top: 10px;">
                    <?php
                        if(empty($_SESSION["email"])){
                    ?>
                    <button style="border: 2px solid white;" class="toggle" data-target = "login-container"> Login </button>
                    <button style="border: 2px solid white; margin-left: 10px;" class="toggle" data-target = "register-container"> Registrati </button>
                    <?php
                        }else{
                    ?>
                        <form action="" method="post">
                            <button name="logout" style="border: 2px solid white; margin-left: 10px;">Logout da <?php echo($_SESSION["email"]); ?></button>
                        </form>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    <div id="navbar" class="flexbox align-center">
        <nav>
            <a href="index.php">Home</a>
            <a href="log.php">Notifiche</a>
            <a href="ordini.php">Acquisti</a>
            <?php
                if(!empty($_SESSION["admin"])){
            ?>
            <a>|</a>
            <a href="dashboard-admin.php">Aggiungi prodotti</a>
            <a href="log_ordini.php">Ordini</a>
            <a href="utenti.php">Utenti</a>
            <?php
                }
            ?>
        </nav>
    </div>
</header>
