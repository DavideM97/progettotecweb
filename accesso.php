<?php
require_once("addNotifica.php");
if($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['emailAcc']) && !empty($_POST['passAcc'])){

  $mail = $_POST['emailAcc'];
  $pass = base64_encode ($_POST['passAcc']);

  $query = "SELECT * FROM utente WHERE email = ? AND password = ?";
  $resultAcc = "";

  $st=$conn->stmt_init();
  if($st->prepare($query)){
    $st->bind_param('ss',$mail,$pass);
    $st->execute();
    $resultAcc = $st->get_result();
  }

  if($resultAcc ->num_rows){
    while ($row = $resultAcc->fetch_assoc()){
      $_SESSION["nome"] = $row["nome"];
      $_SESSION["email"] = $row["email"];
      if($row["admin"] == 1){
        $_SESSION["admin"] = 1;
      }
    }
  }
  add_notifica("login",$conn);
}
?>
