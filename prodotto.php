<!DOCTYPE html>
<html>
<?php

if(session_status()==PHP_SESSION_NONE){
  session_start();
}

if(!isset($_GET["id"])){
  header("location:./index.php");
}
require_once("connessione.php");
require_once("addNotifica.php");
require_once("registrazione.php");
require_once("accesso.php");
require_once("logout.php");
require_once("addToCart.php");
?>
<?php include './headIncl.php'; ?>

<body>
    <div id="mobile-navbar" class="slider show">
        <button><i class="fas fa-times slideOut" data-target = "mobile-navbar"></i></button>
    </div>
    <?php include './forms.php'; ?>
    <?php include './header.php'; ?>
    <div id="main">
        <?php
            mysqli_set_charset($conn, "utf8mb4");
            $id = $_GET["id"];
            $query = $conn ->query("SELECT * FROM prodotto WHERE id = $id");
            if($query ->num_rows){
                while ($row = $query->fetch_assoc()){
        ?>
        <h1 id="main-title"><?php echo($row["nome"]); ?></h1>
        <button class="show" style="margin: 0 auto; margin-bottom: 40px;" href="#index.php">Home</button>
        <div class="flexbox align-top" id="f">
            <div class="prod-image" style="padding-right:25px">
                <img src="images/<?php echo($row["immagine"]); ?>.jpg" />
            </div>
            <div style="flex: 1" id="padded">
                <h2> Descrizione articolo </h2>
                <p style="text-align: justify; font-size: 1.15em;">
                    <?php
                        echo($row["descrizione"]);
                    ?>
                </p>
                <h3> Prezzo:  <?php echo($row["prezzo"]); ?>€ </h3>
                <div class="flexbox">
                    <form method="post" action="">
                        <button type=submit name="addToCart">Aggiungi al carrello</button>
                        <input class="bordered" min="1" type="number" style="margin-top: 10px;" value=1 name="qnt"/>
                        <input type="hidden" value="<?php echo($id)?>" name="id_prod">
                    </form>
                </div>
            </div>
        </div>
        <?php
                }
            }
        ?>
    </div>
    <?php include './footer.php'; ?>
    <?php include './closeConn.php'; ?>
</body>
<style>
    @media screen and (max-width: 850px){
        #f{ display: block; }
        #main .prod-image{ width: 80vw; margin: 0 auto; }
        #padded{ padding: 40px; }
    }
</style>
</html>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>
