<DOCTYPE  !html>
<html>
<?php include './headIncl.php'; ?>
<?php
if(session_status()==PHP_SESSION_NONE){
    session_start();
  }
    if(empty($_SESSION["admin"])){
        header("location:/sito/index.php");
    }
    require_once("connessione.php");
    require_once("addNotifica.php");
    require_once("registrazione.php");
    require_once("accesso.php");
    require_once("logout.php");
?>
<body>
<?php include './forms.php'; ?>
<?php include './header.php'; ?>
<div id="main">
    <h1 id="main-title"> Tutti gli ordini </h1>
    <div class="table-container">
	<table>
		<thead>
			<tr>
                <th>Numero ordine</th>
                <th>Email utente</th>
                <th>Data ordine</th>
			</tr>
		</thead>
		<tbody>
			<?php
                $query = $conn->query("SELECT * FROM ordine ORDER BY data_ordine");
                if($query ->num_rows){
                    while ($row = $query->fetch_assoc()){
                        $id = $row["id"];
                        $email_utente = $row["mail_utente"];
                        $data_ordine = $row["data_ordine"];
                        echo("
                            <tr>
                                <td>$id</td>
                                <td>$email_utente</td>
                                <td>$data_ordine</td>
                            </tr>
                        ");
                    }
                }
            ?>
		</tbody>
	</table>
</div>
</div>
<?php include './footer.php'; ?>
<?php include './closeConn.php';?>
</body>
</html>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>
