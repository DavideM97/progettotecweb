<DOCTYPE  !html>
<html>
<?php include './headIncl.php'; ?>
<?php
if(session_status()==PHP_SESSION_NONE){
    session_start();
  }
    if(empty($_SESSION["email"])){
        header("location:/sito/index.php");
    }
    require_once("connessione.php");
    require_once("addNotifica.php");
    require_once("registrazione.php");
    require_once("accesso.php");
    require_once("logout.php");
?>
<body>
<?php include './forms.php'; ?>
<?php include './header.php'; ?>
<div id="main">
    <h1 id="main-title"> Le tue notifiche </h1>
    <div class="table-container">
	<table>
        <thead>
			<tr>
				<th>Messaggio</th>
				<th>Data</th>

			</tr>
		</thead>
		<tbody>
			<?php
                $mail_utente = $_SESSION["email"];
                $query = $conn->query("SELECT * FROM notifica WHERE mail_utente = '$mail_utente' ORDER BY data_inserimento DESC");
                echo($conn->error);
                if($query ->num_rows){
                    while ($row = $query->fetch_assoc()){
                        $tipo = $row["tipologia"];
                        $data = $row["data_inserimento"];
                        if($tipo == "registrazione"){
                            $msg = "Account creato";
                        }else if($tipo == "login"){
                            $msg ="Accesso all'account effettuato";
                        }else if($tipo == "logout"){
                            $msg ="Disconnessione dall'account effettuata";
                        }else if($tipo == "aggiunto_carrello"){
                            $msg ="Aggiunto prodotto al carrello";
                        }else if($tipo == "rimosso_carrello"){
                            $msg ="Rimosso prodotto dal carrello";
                        }else if($tipo == "checkout"){
                            $msg ="Ordine effettuato, controlla nella pagina di riepilogo ordini per i dettagli";
                        }else if($tipo == "prodotto_creato"){
                            $msg ="Creato nuovo prodotto e aggiunto al database";
                        }
                        echo("
                            <tr>
                                <td>$msg</td>
                                <td>$data</td>
                            </tr>
                        ");
                    }
                }
            ?>
		</tbody>
	</table>
</div>
</div>
<?php include './footer.php'; ?>
<?php include './closeConn.php';?>
</body>
</html>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>
