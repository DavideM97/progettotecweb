<!DOCTYPE html>
<html>
<?php

if(session_status()==PHP_SESSION_NONE){
  session_start();
}
require_once("connessione.php");
require_once("addNotifica.php");
require_once("registrazione.php");
require_once("accesso.php");
require_once("logout.php");
require_once("addToCart.php");
?>
<?php include './headIncl.php'; ?>
  <body>
    <div id="mobile-navbar" class="slider show">
        <button><i class="fas fa-times slideOut" data-target = "mobile-navbar"></i></button>
    </div>
    <div id="filters" class="slider show">
          <button><i class="fas fa-times slideOut" data-target = "filters"></i></button>
        <?php include './sidebar.php'; ?>
    </div>
    <?php include './forms.php'; ?>
    <?php include './header.php'; ?>
    <div id="main">
      <div class="content">
        <div class="header">
          <h2>Acquisto Completato</h2>
        </div>
        <p style="width:100%;"><span class="fas fa-check"></span></p>
        <p>L'acquisto è stato eseguito con successo e procederemo ora alla spedizione!</p>
      </div>
    </div>
    <?php include './footer.php'; ?>
    <?php include './closeConn.php'?>
  </body>
</html>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>
