<!DOCTYPE html>
<html>
    <?php

if(session_status()==PHP_SESSION_NONE){
  session_start();
}
require_once("connessione.php");
require_once("addNotifica.php");
require_once("registrazione.php");
require_once("accesso.php");
require_once("logout.php");
require_once("addToCart.php");
?>
<?php include './headIncl.php'; ?>

<body>
    <div id="mobile-navbar" class="slider show">
        <button><i class="fas fa-times slideOut" data-target = "mobile-navbar"></i></button>
    </div>
    <?php include './sidebar.php'; ?>
    <?php include './forms.php'; ?>
    <?php include './header.php'; ?>
    <div id="main">
        <h1 id="main-title"> Risultati per: <?php echo($_GET["q"]) ?></h1>
        <button class="show slideIn" style="margin: 0 auto; margin-bottom: 40px;" data-target = "filters">Filtri</button>
        <div class="flexbox align-top" id="main-grid">
            <div class="sidebar" id="main-sidebar">
                <?php include './sidebar.php'; ?>
            </div>
            <div class="main-view flexbox wrap" id="main-flexbox">
                <?php
                    if(isset($_GET["q"])){
                      $ricerca = strtolower("%{$_GET["q"]}%");
                      $resultRicerca = "";
                      if(isset($_POST["filterCat"])){
                        $id_cat_filter = $_POST["cat"];
                        $query = "SELECT * FROM prodotto WHERE lower(nome) LIKE ? AND id_cat = ? ORDER BY nome";

                        $st=$conn->stmt_init();
                        if($st->prepare($query)){
                          $st->bind_param('si',$ricerca,$id_cat_filter);
                          $st->execute();
                          $resultRicerca = $st->get_result();
                        }

                    }else if(isset($_POST["filterSotCat"])){
                        $id_sotcat_filter = $_POST["sotcat"];
                        $query = "SELECT * FROM prodotto WHERE lower(nome) LIKE ? AND id_cat = ? ORDER BY nome";

                        $st=$conn->stmt_init();
                        if($st->prepare($query)){
                          $st->bind_param('si',$ricerca,$id_sotcat_filter);
                          $st->execute();
                          $resultRicerca = $st->get_result();
                        }
                    }else{
                      $query = "SELECT * FROM prodotto WHERE lower(nome) LIKE ? ORDER BY nome";

                      $st=$conn->stmt_init();
                      if($st->prepare($query)){
                        $st->bind_param('s',$ricerca);
                        $st->execute();
                        $resultRicerca = $st->get_result();
                      }
                    }
                      if($resultRicerca ->num_rows){
                          while ($row = $resultRicerca->fetch_assoc()){
                              $id_prod = $row["id"];
                              $nome = $row["nome"];
                              $descr = $row["descrizione"];
                              $img = $row["immagine"] . ".jpg";
                              $prezzo = $row["prezzo"];
                              echo("
                              <div class=\"item\">
                                    <a href=\"prodotto.php?id=$id_prod\">
                                      <img src=\"images/$img\" alt=\"$nome\">
                                  </a>
                                  <p>$nome</p>
                                  <h2>$prezzo €</h2>
                                  <form method=\"post\">
                                      <button type=\"submit\" name=\"addToCart\"> Aggiungi al carrello </button>
                                      <input type=\"hidden\" value=\"1\" name=\"qnt\">
                                      <input type=\"hidden\" value=\"$id_prod\" name=\"id_prod\">
                                  </form>
                              </div>
                              ");
                          }
                      }
                    }
                ?>
            </div>
        </div>
    </div>
    <?php include './footer.php'; ?>
    <?php include './closeConn.php'; ?>
</body>

</html>

<script>
    let dropdown_parent = document.getElementsByClassName('dropdown');
    Array.from(dropdown_parent)
        .forEach(item => {

            let children = Array.from(item.children);
            let event = Array.from(children[0].children);
            console.log(event[1]);

            event[1].addEventListener('click', (e) => {

                let icon = Array.from(children[0].children)[1]

                if (children[1].classList.contains('dropped')) {

                    icon.style.transform = 'rotate(0)'
                }
                else {
                    icon.style.transform = 'rotate(180deg)'
                }

                for(var i=1; i<children.length; i++){
                    if (children[i].classList.contains('dropped')) {
                        children[i].style.display = 'none';
                        children[i].classList.remove('dropped');
                    }else{
                        children[i].style.display = 'block';
                        children[i].classList.add('dropped');
                    }
                }

            })

        })

</script>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>
