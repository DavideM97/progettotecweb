<div class="modal-container flexbox align-center justify-center" id="login-container">
    <div class="modal fade" id="accedi" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                      <button type="button" class="toggle" data-target = "login-container">X</button>
                    <h4 class="modal-title">Accedi a Plantemall</h4>
                </div>
                <div id="errorMessage"></div>
                <div class="modal-body">
                    <form action="" method="post" onSubmit="return validateUser();">
                        <input type="email" placeholder="Email" class="bordered form-control" name="emailAcc" id="emailAccedi">
                        <input type="password" placeholder="Password" class="bordered form-control" name="passAcc" id="passwordAccedi">
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Accedi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-container flexbox align-center justify-center" id="register-container">
    <div class="modal fade" id="registrazione" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="toggle" data-target = "register-container">X</button>
                    <h4 class="modal-title">Registrati a Plantemall</h4>
                </div>
                <div id="errorMessage2"></div>
                <div class="modal-body">
                    <form action="" method="post" onSubmit="return registrationUser();">
                        <input type="text" placeholder="Nome" class="bordered form-control" name="namereg" id="nomeregCli">
                        <input type="email" placeholder="Email" class="bordered form-control" name="emailreg" id="emailregCli">
                        <input type="password" placeholder="Password" class="bordered form-control" name="passwordreg" id="passwordregCli">
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Registrati</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
