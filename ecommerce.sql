-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Lug 13, 2021 alle 17:03
-- Versione del server: 10.4.19-MariaDB
-- Versione PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--
CREATE DATABASE IF NOT EXISTS `ecommerce` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ecommerce`;

-- --------------------------------------------------------

--
-- Struttura della tabella `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `id_prod` int(11) NOT NULL,
  `mail_utente` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `quantita` int(11) NOT NULL,
  `id_ordine` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `cart`
--

INSERT INTO `cart` (`id`, `id_prod`, `mail_utente`, `quantita`, `id_ordine`) VALUES
(9, 1, 'bho@bho.com', 2, 6),
(7, 1, 'bho@bho.com', 1, 5),
(10, 3, 'bho@bho.com', 1, 6),
(11, 2, 'bho@bho.com', 1, 7),
(12, 1, 'bho@bho.com', 2, 8),
(13, 15, 'bho@bho.com', 1, 8),
(14, 1, 'carlo@carlo.it', 1, 9),
(20, 1, 'carlo@carlo.it', 1, 11),
(19, 1, 'bho@bho.com', 1, 10),
(21, 3, 'carlo@carlo.it', 1, 12),
(23, 1, 'bho@bho.com', 15, 13),
(24, 3, 'bho@bho.com', 1, 13),
(25, 9, 'bho@bho.com', 1, 13),
(28, 25, 'bho@bho.com', 1, 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `categoria`
--

INSERT INTO `categoria` (`id`, `nome`) VALUES
(1, 'Alberi da Frutto'),
(2, 'Conifere'),
(3, 'Latifoglie'),
(4, 'Piante Grasse');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifica`
--

CREATE TABLE `notifica` (
  `id` int(11) NOT NULL,
  `mail_utente` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tipologia` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_inserimento` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `notifica`
--

INSERT INTO `notifica` (`id`, `mail_utente`, `tipologia`, `data_inserimento`) VALUES
(1, 'bho@bho.com', 'login', '2021-06-21 15:54:19'),
(2, 'bho@bho.com', 'aggiunto_carrello', '2021-06-21 15:54:32'),
(3, 'bho@bho.com', 'aggiunto_carrello', '2021-06-21 15:54:45'),
(4, 'bho@bho.com', 'login', '2021-06-24 15:00:01'),
(5, 'bho@bho.com', 'login', '2021-06-24 15:00:15'),
(6, 'bho@bho.com', 'login', '2021-06-24 15:00:59'),
(7, 'bho@bho.com', 'checkout', '2021-06-24 16:42:46'),
(8, 'bho@bho.com', 'login', '2021-06-26 11:06:52'),
(9, 'bho@bho.com', 'login', '2021-06-26 11:07:00'),
(10, 'bho@bho.com', 'aggiunto_carrello', '2021-06-26 11:07:12'),
(11, 'bho@bho.com', 'checkout', '2021-06-26 11:07:34'),
(12, 'bho@bho.com', 'logout', '2021-06-26 11:19:58'),
(13, 'bho@bho.com', 'login', '2021-06-26 11:20:40'),
(14, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:25:31'),
(15, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:28:21'),
(16, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:30:26'),
(17, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:32:37'),
(18, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:35:04'),
(19, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:38:50'),
(20, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:40:42'),
(21, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:42:23'),
(22, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:44:56'),
(23, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:47:41'),
(24, 'bho@bho.com', 'login', '2021-06-26 11:49:29'),
(25, 'bho@bho.com', 'aggiunto_carrello', '2021-06-26 12:03:12'),
(26, 'bho@bho.com', 'aggiunto_carrello', '2021-06-26 12:03:22'),
(27, 'bho@bho.com', 'login', '2021-07-02 17:29:45'),
(28, 'bho@bho.com', 'login', '2021-07-02 17:30:04'),
(29, 'bho@bho.com', 'logout', '2021-07-02 17:45:43'),
(30, 'bho@bho.com', 'login', '2021-07-02 18:02:19'),
(31, 'bho@bho.com', 'aggiunto_carrello', '2021-07-02 18:02:21'),
(32, 'bho@bho.com', 'checkout', '2021-07-02 18:02:24'),
(33, 'bho@bho.com', 'logout', '2021-07-02 18:02:28'),
(34, 'davide@davide.it', 'registrazione', '2021-07-02 18:03:55'),
(35, 'davide@davide.it', 'logout', '2021-07-02 18:04:21'),
(36, 'davide@davide.it', 'registrazione', '2021-07-02 18:04:30'),
(37, 'davide@davide.it', 'logout', '2021-07-02 18:04:35'),
(38, 'davide@davide.it', 'login', '2021-07-02 18:04:48'),
(39, 'davide@davide.it', 'logout', '2021-07-02 18:11:11'),
(40, 'davide@davide.it', 'registrazione', '2021-07-02 18:19:38'),
(41, 'davide@davide.it', 'logout', '2021-07-02 18:19:54'),
(42, 'carlo@carlo.it', 'registrazione', '2021-07-04 16:15:15'),
(43, 'carlo@carlo.it', 'logout', '2021-07-04 16:15:16'),
(44, 'carlo@carlo.it', 'registrazione', '2021-07-04 16:15:25'),
(45, 'carlo@carlo.it', 'logout', '2021-07-04 16:15:43'),
(46, 'bho@bho.com', 'login', '2021-07-04 16:27:28'),
(47, 'bho@bho.com', 'logout', '2021-07-04 16:27:29'),
(48, 'bho@bho.com', 'login', '2021-07-04 16:27:34'),
(49, 'bho@bho.com', 'logout', '2021-07-04 16:34:17'),
(50, 'bho@bho.com', 'login', '2021-07-05 17:34:05'),
(51, 'bho@bho.com', 'logout', '2021-07-05 17:34:06'),
(52, 'bho@bho.com', 'login', '2021-07-05 17:34:14'),
(53, 'bho@bho.com', 'logout', '2021-07-05 17:34:16'),
(54, 'carlo@carlo.it', 'login', '2021-07-05 17:34:20'),
(55, 'carlo@carlo.it', 'logout', '2021-07-05 17:34:21'),
(56, 'bho@bho.com', 'login', '2021-07-05 17:40:14'),
(57, 'bho@bho.com', 'logout', '2021-07-05 17:40:25'),
(58, 'bho@bho.com', 'login', '2021-07-06 13:28:37'),
(59, 'bho@bho.com', 'logout', '2021-07-06 13:34:43'),
(60, 'bho@bho.com', 'login', '2021-07-06 13:34:53'),
(61, 'bho@bho.com', 'logout', '2021-07-06 13:34:54'),
(62, 'bho@bho.com', 'login', '2021-07-06 13:35:00'),
(63, 'bho@bho.com', 'logout', '2021-07-06 13:35:06'),
(64, 'bho@bho.com', 'login', '2021-07-06 17:23:16'),
(65, 'bho@bho.com', 'logout', '2021-07-06 17:23:19'),
(66, 'bho@bho.com', 'login', '2021-07-06 17:29:39'),
(67, 'bho@bho.com', 'logout', '2021-07-06 17:29:41'),
(68, 'carlo@carlo.it', 'login', '2021-07-06 17:58:24'),
(69, 'carlo@carlo.it', 'aggiunto_carrello', '2021-07-06 17:58:26'),
(70, 'carlo@carlo.it', 'checkout', '2021-07-06 17:58:34'),
(71, 'carlo@carlo.it', 'aggiunto_carrello', '2021-07-06 17:58:40'),
(72, 'carlo@carlo.it', 'rimosso_carrello', '2021-07-06 17:58:46'),
(73, 'carlo@carlo.it', 'logout', '2021-07-06 17:59:41'),
(74, 'bho@bho.com', 'login', '2021-07-06 18:01:36'),
(75, 'bho@bho.com', 'aggiunto_carrello', '2021-07-06 18:01:37'),
(76, 'bho@bho.com', 'aggiunto_carrello', '2021-07-06 18:01:41'),
(77, 'bho@bho.com', 'aggiunto_carrello', '2021-07-06 18:01:41'),
(78, 'bho@bho.com', 'rimosso_carrello', '2021-07-06 18:01:44'),
(79, 'bho@bho.com', 'rimosso_carrello', '2021-07-06 18:01:45'),
(80, 'bho@bho.com', 'rimosso_carrello', '2021-07-06 18:01:45'),
(81, 'bho@bho.com', 'logout', '2021-07-06 18:05:17'),
(82, 'bho@bho.com', 'login', '2021-07-06 18:05:34'),
(83, 'bho@bho.com', 'login', '2021-07-06 18:05:40'),
(84, 'bho@bho.com', 'logout', '2021-07-06 18:05:56'),
(85, 'bho@bho.com', 'logout', '2021-07-06 18:07:26'),
(86, 'bho@bho.com', 'login', '2021-07-06 18:07:30'),
(87, 'bho@bho.com', 'aggiunto_carrello', '2021-07-06 18:07:33'),
(88, 'bho@bho.com', 'checkout', '2021-07-06 18:07:34'),
(89, 'bho@bho.com', 'logout', '2021-07-06 18:13:06'),
(90, 'bho@bho.com', 'login', '2021-07-06 18:14:18'),
(91, 'bho@bho.com', 'logout', '2021-07-06 18:14:20'),
(92, 'bho@bho.com', 'login', '2021-07-06 18:14:28'),
(93, 'bho@bho.com', 'logout', '2021-07-06 18:14:32'),
(94, 'davide1@sos.it', 'registrazione', '2021-07-06 18:19:04'),
(95, 'davide1@sos.it', 'logout', '2021-07-06 18:19:06'),
(96, 'lorenzo@davide.it', 'registrazione', '2021-07-06 18:19:19'),
(97, 'lorenzo@davide.it', 'logout', '2021-07-06 18:19:24'),
(98, 'bho@bho.com', 'login', '2021-07-06 18:29:33'),
(99, 'bho@bho.com', 'login', '2021-07-06 18:30:53'),
(100, 'bho@bho.com', 'logout', '2021-07-06 18:38:35'),
(101, 'davide@c.it', 'registrazione', '2021-07-06 18:38:52'),
(102, 'davide@c.it', 'logout', '2021-07-06 18:38:54'),
(103, 'carlo@carlo.it', 'login', '2021-07-06 18:39:15'),
(104, 'carlo@carlo.it', 'aggiunto_carrello', '2021-07-06 18:39:20'),
(105, 'carlo@carlo.it', 'checkout', '2021-07-06 18:39:23'),
(106, 'carlo@carlo.it', 'aggiunto_carrello', '2021-07-06 18:39:28'),
(107, 'carlo@carlo.it', 'checkout', '2021-07-06 18:39:31'),
(108, 'bho@bho.com', 'aggiunto_carrello', '2021-07-07 17:48:16'),
(109, 'davide@davide.it', 'registrazione', '2021-07-10 09:18:26'),
(110, 'davide@davide.it', 'logout', '2021-07-10 09:19:08'),
(111, 'bho@bho.com', 'logout', '2021-07-10 09:40:55'),
(112, 'bho@bho.com', 'logout', '2021-07-10 09:41:31'),
(113, 'bho@bho.com', 'login', '2021-07-10 09:42:39'),
(114, 'bho@bho.com', 'logout', '2021-07-10 09:42:44'),
(115, 'bho@bho.com', 'login', '2021-07-10 09:42:53'),
(116, 'bho@bho.com', 'logout', '2021-07-10 09:42:55'),
(117, 'bho@bho.com', 'login', '2021-07-10 09:42:59'),
(118, 'bho@bho.com', 'logout', '2021-07-10 09:43:00'),
(119, 'davide@davide.it', 'registrazione', '2021-07-10 09:43:13'),
(120, 'davide@davide.it', 'logout', '2021-07-10 09:43:17'),
(121, 'bho@bho.com', 'login', '2021-07-10 09:43:23'),
(122, 'bho@bho.com', 'aggiunto_carrello', '2021-07-10 09:43:24'),
(123, 'bho@bho.com', 'checkout', '2021-07-10 09:43:27'),
(124, 'bho@bho.com', 'logout', '2021-07-10 09:43:34'),
(125, 'bho@bho.com', 'login', '2021-07-10 09:43:56'),
(126, 'bho@bho.com', 'aggiunto_carrello', '2021-07-10 09:48:31'),
(127, 'bho@bho.com', 'prodotto_creato', '2021-07-10 11:02:29'),
(128, 'bho@bho.com', 'prodotto_creato', '2021-07-10 11:02:59'),
(129, 'bho@bho.com', 'login', '2021-07-12 15:19:49'),
(130, 'bho@bho.com', 'logout', '2021-07-12 15:20:11'),
(131, 'bho@bho.com', 'login', '2021-07-12 15:24:12'),
(132, 'bho@bho.com', 'prodotto_creato', '2021-07-12 15:35:27'),
(133, 'bho@bho.com', 'prodotto_creato', '2021-07-12 15:36:10'),
(134, 'bho@bho.com', 'prodotto_creato', '2021-07-12 15:36:40'),
(135, 'bho@bho.com', 'prodotto_creato', '2021-07-12 15:37:03'),
(136, 'bho@bho.com', 'logout', '2021-07-12 15:38:42'),
(137, 'davide@davide.it', 'registrazione', '2021-07-12 15:56:51'),
(138, 'davide@davide.it', 'logout', '2021-07-12 15:57:00'),
(139, 'bho@bho.com', 'login', '2021-07-12 16:02:59'),
(140, 'bho@bho.com', 'login', '2021-07-12 16:07:04'),
(141, 'bho@bho.com', 'login', '2021-07-12 16:07:17'),
(142, 'bho@bho.com', 'login', '2021-07-12 16:08:29'),
(143, 'bho@bho.com', 'login', '2021-07-12 16:10:15'),
(144, 'bho@bho.com', 'aggiunto_carrello', '2021-07-12 16:14:56'),
(145, 'bho@bho.com', 'aggiunto_carrello', '2021-07-12 16:14:59'),
(146, 'bho@bho.com', 'aggiunto_carrello', '2021-07-12 16:15:00'),
(147, 'bho@bho.com', 'rimosso_carrello', '2021-07-12 16:15:01'),
(148, 'bho@bho.com', 'rimosso_carrello', '2021-07-12 16:15:02'),
(149, 'bho@bho.com', 'logout', '2021-07-12 16:15:12'),
(150, 'bho@bho.com', 'login', '2021-07-12 16:22:12'),
(151, 'bho@bho.com', 'logout', '2021-07-12 16:22:18'),
(152, 'bho@bho.com', 'login', '2021-07-12 16:22:50'),
(153, 'bho@bho.com', 'logout', '2021-07-12 16:22:57'),
(154, 'bho@bho.com', 'login', '2021-07-12 16:26:49'),
(155, 'bho@bho.com', 'prodotto_creato', '2021-07-12 16:43:24'),
(156, 'bho@bho.com', 'prodotto_creato', '2021-07-12 16:44:49'),
(157, 'bho@bho.com', 'aggiunto_carrello', '2021-07-12 16:46:07'),
(158, 'bho@bho.com', 'checkout', '2021-07-12 16:46:10'),
(159, 'bho@bho.com', 'logout', '2021-07-12 16:49:00'),
(160, 'bho@bho.com', 'login', '2021-07-12 16:51:04'),
(161, 'bho@bho.com', 'logout', '2021-07-12 16:53:40'),
(162, 'bho@bho.com', 'login', '2021-07-13 17:01:27');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `id` int(11) NOT NULL,
  `mail_utente` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_ordine` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`id`, `mail_utente`, `data_ordine`) VALUES
(6, 'bho@bho.com', '2021-06-24 00:00:00'),
(5, 'bho@bho.com', '2021-06-23 00:00:00'),
(7, 'bho@bho.com', '2021-06-26 11:07:32'),
(8, 'bho@bho.com', '2021-07-02 18:02:24'),
(9, 'carlo@carlo.it', '2021-07-06 17:58:34'),
(10, 'bho@bho.com', '2021-07-06 18:07:34'),
(11, 'carlo@carlo.it', '2021-07-06 18:39:23'),
(12, 'carlo@carlo.it', '2021-07-06 18:39:31'),
(13, 'bho@bho.com', '2021-07-10 09:43:27'),
(14, 'bho@bho.com', '2021-07-12 16:46:10');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descrizione` text CHARACTER SET utf8mb4 NOT NULL,
  `immagine` varchar(255) NOT NULL,
  `prezzo` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL,
  `id_sottocat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`id`, `nome`, `descrizione`, `immagine`, `prezzo`, `id_cat`, `id_sottocat`) VALUES
(1, 'Albicocca Goldrich', 'Albero vigoroso, di media produttivita\'. \r\nIl frutto e\' di calibro grosso, con buccia di colore arancio intenso; \r\nmolto dolce e gustoso, soprattutto a maturazione avanzata.\r\nE\' conosciuta anche con il nome \"Sungiant\". \r\nE\' una delle varietà più coltivate in quanto i frutti riescono a tenere \r\nmolto bene sulla pianta, così da consentire un periodo di raccolta \r\npiù lungo. \r\nLa raccolta avviene, di solito, nella seconda metà del mese di giugno. \r\nI frutti sono utilizzati per il consumo fresco.', 'Albicocca_Goldrich', 1300, 1, 1),
(2, 'Albicocca Reale d\'Imola', 'E\' una delle varieta\' più conosciute, la pianta e\' di buon vigore\r\ncon abbondanti produzioni, i frutti hanno forma allungata e sono \r\ngrossi con buccia gialla a sfumature rossastre, la polpa e giallo\r\nchiaro molto dolce e profumata. \r\nMatura da meta\' giugno ai primi di luglio.\r\nE\' una pianta che si distingue per una produttività non costante; \r\nessa si dimostra più abbondante al Nord Italia, \r\nmentre e\' media al centro e al sud. \r\nSe la pianta non viene sottoposta a diradamento la \r\nproduttività va a scalare e dura all\'incirca tre settimane.', 'Albicocca_Reale', 1000, 1, 1),
(3, 'Albicocco Ivonne Liverani', 'Albero di media vigoria a portamento intermedio. \r\nE’ caratterizzato da un elevata resistenza alle malattie e \r\nalle gelate primaverili. \r\nLa sua produttività risulta elevata e costante infatti la pianta \r\nfruttifica sui dardi e rami misti.\r\nFiorisce precocemente e i frutti sono medi a forma ovata, \r\ncon buccia color giallo-arancio e polpa aranciata. \r\nSapore dolce e di breve conservazione.\r\nMaturazione 20-30 giugno.', 'Albicocco_Ivonne_Liverani', 1250, 1, 1),
(9, 'Ciliegia Bigarreau Burlat', 'Varieta\' di origine francese, con albero dal portamento assurgente \r\nespanso, caratterizzato da vigoria e produttività media.\r\nIl frutto e\' di pezzatura buona, di forma a cuore, \r\ncon peduncolo di media lunghezza; \r\nla buccia e\' di colore rosso cupo e polpa rossa di consistenza \r\nmedia elevata, buona succosita\'; sapore dolce e conservazione breve.\r\nMaturazione: 20-30 maggio.', 'Ciliegia_Bigarreau_Burlat', 1350, 1, 2),
(15, 'Ciliegia Bigarreau Moreau', 'Cultivar francese molto presente in Emilia Romagna; \r\nse ne conoscono due tipi, quella disponibile è \r\nil tipo B che produce frutti grandi cuoriformi \r\ncon buccia rosso cupo e polpa rossa scura.\r\nIl peduncolo di media lunghezza e il nocciolo è grosso. \r\nFiorisce precocemente ed è autoincompatibile. \r\nViene impollinata da Mora di Vignola, Durone nero I, \r\nBigarreau Burlat. Cultivar produttiva con una precoce \r\nmessa a frutto di sapore dolce e conservazione breve.\r\nMaturazione: 20-30 maggio.', 'Ciliegia_Bigarreau_Moreau', 2000, 1, 2),
(16, 'Ciliegia Durona di Vignola', 'Famosa varietà conosciuta per il suo grosso calibro di \r\nun colore rosso scuro quasi nero; \r\ndeve il suo nome alla polpa consistente che le ha permesso \r\ndi essere coltivata anche in grande scala perché adatta ai trasporti.\r\nE’ una varietà vigorosa produttiva, per l’impollinazione sono \r\nadatte Bigarreau Moreau, Bigarreau Burlat.\r\nNel tempo ne sono stati selezionati diversi cloni che \r\ndifferiscono principalmente per l’epoca di maturazione, \r\nquello che noi proponiamo è a maturazione intermedia \r\ne cioè a metà giugno.', 'Ciliegia_Durona_di_Vignola', 1860, 1, 2),
(25, 'Acer palmatum “Anne Irene”', 'La pianta di Acer palmatum “Anne Irene” è un arbusto deciduo, appartenente alla famiglia delle Sapindaceae/Aceraceae ed originario dell’estremo Oriente, comunemente chiamato Acero Giapponese. Sviluppa una chioma arrotondata con foglie decorative, dai lobi molto divisi e frastagliati. Le foglie, di colore verde brillante e margini evidenziati da un colore rossastro brillante, diventano verde scuro in estate e giallo-arancio in autunno. Fiori di colore rosso porpora, sono riuniti in piccoli grappoli che fioriscono in estate. Pianta rustica resistente al freddo (fino a -15° C), predilige terreni ricchi e ben drenati. Non richiede molto spazio e può essere sistemata anche in zone d’angolo a mezzombra. Coltivata come pianta ornamentale, singola o in gruppi, è ideale in grandi parchi e piccoli giardini, si adatta alla coltivazione in vasi e fioriere.', 'Acero_Palmato_Anne_Irene', 50, 3, 8);

-- --------------------------------------------------------

--
-- Struttura della tabella `sottocategoria`
--

CREATE TABLE `sottocategoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `id_cat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `sottocategoria`
--

INSERT INTO `sottocategoria` (`id`, `nome`, `id_cat`) VALUES
(1, 'Albicocchi', 1),
(2, 'Ciliegi', 1),
(3, 'Peschi', 1),
(4, 'Abeti', 2),
(5, 'Cedri', 2),
(6, 'Cipressi', 2),
(7, 'Acacie', 3),
(8, 'Aceri', 3),
(9, 'Cactacee', 4),
(10, 'Succulente', 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_registrazione` datetime NOT NULL,
  `admin` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`email`, `nome`, `password`, `data_registrazione`, `admin`) VALUES
('bho@bho.com', 'Dennis Cavina', 'MTIzc3RlbGxh', '2021-06-16 10:22:13', 1),
('carlo@carlo.it', 'Carlo', 'Y2FybG8=', '2021-07-04 16:15:15', 0),
('davide@davide.it', 'Davide', 'ZGF2aWRl', '2021-07-10 09:43:13', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `notifica`
--
ALTER TABLE `notifica`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `sottocategoria`
--
ALTER TABLE `sottocategoria`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT per la tabella `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `notifica`
--
ALTER TABLE `notifica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT per la tabella `sottocategoria`
--
ALTER TABLE `sottocategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
