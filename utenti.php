<DOCTYPE  !html>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/60e43df7e9.js" crossorigin="anonymous"></script>
    <link href="./style.css" rel="stylesheet" type="text/css">
</head>
<?php
if(session_status()==PHP_SESSION_NONE){
    session_start();
  }
    if(empty($_SESSION["admin"])){
        header("location:/sito/index.php");
    }
    require_once("connessione.php");
    require_once("addNotifica.php");
    require_once("registrazione.php");
    require_once("accesso.php");
    require_once("logout.php");
?>
<body>
<?php include './forms.php'; ?>
<?php include './header.php'; ?>
<div id="main">
    <h1 id="main-title"> Tutti gli utenti </h1>
    <div class="table-container">
	<table>
		<thead>
			<tr>
                <th>Email</th>
                <th>Nome</th>
                <th>Data Registrazione</th>
			</tr>
		</thead>
		<tbody>
			<?php
                //$conn = connection();
                $query = $conn->query("SELECT * FROM utente ORDER BY data_registrazione");
                if($query ->num_rows){
                    while ($row = $query->fetch_assoc()){
                        $email = $row["email"];
                        $nome = $row["nome"];
                        $data = $row["data_registrazione"];
                        echo("
                            <tr>
                                <td>$email</td>
                                <td>$nome</td>
                                <td>$data</td>
                            </tr>
                        ");
                    }
                }
            ?>
		</tbody>
	</table>
</div>
</div>
    <?php include './footer.php'; ?>
    <?php include './closeConn.php'?>
</body>
</html>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>
