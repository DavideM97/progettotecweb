<head lang="it">
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="./jquery-1.11.3.min.js"></script>
    <script src="https://kit.fontawesome.com/60e43df7e9.js" crossorigin="anonymous"></script>
    <link href="./style.css" rel="stylesheet" type="text/css">
</head>
