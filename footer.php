<footer class ="footer">
    <div class ="footer-left">
        <h1>Plantemall</h1>
        <p>Siamo specializzati in vendita di svariati tipi di piante, il sito è stato creato per rendere possibile, anche durante la
            pandemia di poter comprare le tue piante preferite comodamente da casa! La spedizione effettuata sarà sempre gratuita
            ed impiegherà al massimo 3 giorni per arrivare a casa tua, in modo da preservare al meglio l'integrità della pianta.
        </p>
        <div class ="socials">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-instagram"></i></a>
        </div>
    </div>
    <ul class="footer-right">
        <li>
            <h1>Indirizzo</h1>
            <ul class ="box">
                <li>Via dei Giardini, 12</li>
                <li>Bassano del Grappa(VI)</li>
                <li>Italia</li>
            </ul>
        </li>
        <li>
            <h1>Orari di apertura</h1>
            <ul class ="box">
                <li>Martedì: 8.30-12.30 14.30-18.30</li>
                <li>Mercoledì: 8.30-12.30 14.30-18.30</li>
                <li>Giovedì: 8.30-12.30 14.30-18.30</li>
                <li>Venerdì: 8.30-12.30 14.30-18.30</li>
                <li>Sabato: 8.30-12.30 14.30-18.30</li>
                <li>Domenica: 8.30-12.30 14.30-18.30</li>
            </ul>
        </li>
    </ul>
    <div class ="footer-bottom">
        <p>
            All Right reserved by davide.manieri@studio.unibo.it
        </p>
    </div>
</footer>
