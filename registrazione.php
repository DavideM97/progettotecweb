<?php
error_reporting(E_ALL);


if($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['namereg']) && !empty($_POST['emailreg']) && !empty($_POST['passwordreg'])){
  $query = "INSERT INTO utente(email,nome,password,data_registrazione) VALUES ( ?, ?, ?, now())";
  $email = $_POST['emailreg'];
  $nome = $_POST['namereg'];
  $pass = base64_encode($_POST['passwordreg']);

  $st=$conn->stmt_init();
  if($st->prepare($query)){
    $st->bind_param('sss',$email,$nome,$pass);
    $st->execute();
  }

  $_SESSION["email"] = $email;
  $_SESSION["nome"] = $nome;
  add_notifica("registrazione",$conn);
}
?>
