<DOCTYPE  !html>
<html>
<?php include './headIncl.php'; ?>
<?php
if(session_status()==PHP_SESSION_NONE){
    session_start();
  }
    if(empty($_SESSION["email"])){
        header("location:/sito/index.php");
    }
    require_once("connessione.php");
    require_once("addNotifica.php");
    require_once("registrazione.php");
    require_once("accesso.php");
    require_once("logout.php");
?>
<body>
<?php include './forms.php'; ?>
<?php include './header.php'; ?>
<div id="main">
    <h1 id="main-title"> I tuoi ordini </h1>
    <?php
            $mail = $_SESSION['email'];
            $query = $conn ->query("SELECT * FROM ordine WHERE mail_utente = '$mail' ORDER BY id DESC");
            if($query ->num_rows){

                while ($row = $query->fetch_assoc()){
                    $id_ordine = $row["id"];
                    echo("
                        <h2 style=\"color: var(--darkest-green);\">Ordine numero: $id_ordine</h2>
                        <ul class=\"fancy-list\">
                        ");

                    $query_cart = $conn ->query("SELECT * FROM cart WHERE id_ordine = $id_ordine");
                    if($query_cart ->num_rows){
                        while($row_cart = $query_cart->fetch_assoc()){
                            $id_prod = $row_cart["id_prod"];
                            $query_prod = $conn ->query("SELECT * FROM prodotto WHERE id = $id_prod");
                            if($query_prod ->num_rows){
                                while($row_prod = $query_prod->fetch_assoc()){
                                    $img = $row_prod["immagine"];
                                    $nome = $row_prod['nome'];
                                    $qnt = $row_cart['quantita'];
                                    $prezzo = $row_prod['prezzo'] * $qnt;
                                    echo("<li class=\"flexbox align-top\">
                                    <div class=\"image\">
                                        <img src=\"images/$img.jpg\" alt=\"$nome\">
                                    </div>
                                    <div>
                                        <h2>$nome</h2>
                                        <p>
                                            Quantità: $qnt<br>
                                            Prezzo: $prezzo €<br>
                                        </p>
                                    </div>
                                </li>");
                                }
                            }
                        }
                    }
                    echo('</ul>');
                }
              }
        ?>
</div>
<?php include './footer.php';?>
<?php include './closeConn.php';?>
</body>
</html>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>
