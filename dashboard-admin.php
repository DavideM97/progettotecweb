<DOCTYPE  !html>
<html lang="it">
<?php include './headIncl.php'; ?>
<?php
if(session_status()==PHP_SESSION_NONE){
    session_start();
  }
    if(empty($_SESSION["admin"])){
        header("location:/sito/index.php");
    }
    require_once("connessione.php");
    require_once("addNotifica.php");
    require_once("registrazione.php");
    require_once("accesso.php");
    require_once("logout.php");
    require_once("aggiungiProdotto.php");
?>
<body>
<?php include './header.php'; ?>
<?php include './forms.php'; ?>
<div id="main">
    <h1 id="main-title"> Tutte le piante </h1>
    <button class="show slideIn" style="margin: 0 auto; margin-bottom: 40px;" data-target = "filters">Filtri</button>
    <div class="flexbox align-top" id="main-grid">
        <div id="addProd">
            <h2> Aggiungi prodotto </h2>
            <ul>
            <form style="width: 100%;" action = "" method = "post">
                <div class="modal-body">
                        <input type="text" placeholder="Nome piantina" class="bordered" name="nome">
                        <input type="text" placeholder="ID Categoria" class="bordered" name="cat">
                        <input type="text" placeholder="ID Sotto Categoria" class="bordered" name="sotcat">
                        <textarea placeholder="Descrizione" class="bordered" style="height: 200px;" name="descr"></textarea>
                        <input type="text" placeholder="Immagine" class="bordered" name="img">
                        <input type="text" placeholder="Prezzo" class="bordered" name="prezzo">
                </div>
                <div class="modal-footer">
                    <button type="submit" name="createProd" class="btn btn-primary align-center">Aggiungi piantina</button>
                </div>
                </form>
            </ul>
        </div>
        <div class="main-view flexbox wrap" id="main-flexbox">
        <?php
                    $query = $conn->query("SELECT * FROM prodotto ORDER BY nome");
                    if($query ->num_rows){
                        while ($row = $query->fetch_assoc()){
                            $id_prod = $row["id"];
                            $nome = $row["nome"];
                            $descr = $row["descrizione"];
                            $img = $row["immagine"] . ".jpg";
                            $prezzo = $row["prezzo"];
                            echo("
                            <div class=\"item\">
                                  <a href=\"prodotto.php?id=$id_prod\" target=\"_blank\">
                                    <img src=\"images/$img\" alt=\"$nome\">
                                </a>
                                <p>$nome</p>
                                <h2>$prezzo €</h2>
                            </div>
                            ");
                        }
                    }
                ?>
        </div>
    </div>
</div>
<?php include './footer.php'; ?>
<?php include './closeConn.php'?>
</body>
</html>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>
