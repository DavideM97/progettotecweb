<!DOCTYPE html>
<html>
<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
if(empty($_SESSION["email"])){
    header("location:/sito/index.php");
}
require_once("connessione.php");
require_once("addNotifica.php");
require_once("registrazione.php");
require_once("accesso.php");
require_once("logout.php");
require_once("removeFromCart.php");
require_once("checkout.php");
?>
<?php include './headIncl.php'; ?>
<body>
<?php include './forms.php'; ?>
<?php include './header.php'; ?>
    <div id="main">
        <div class="flexbox align-center">
            <h1 id="main-title"> Carrello </h1>
            <form action="" method="post">
                <button name="checkout" type="submit">Checkout</button>
            </form>
        </div>
        <?php
            $mail = $_SESSION['email'];
            $query = $conn ->query("SELECT * FROM cart WHERE mail_utente = '$mail' AND id_ordine IS NULL");
            if($query ->num_rows){
                echo('
                <ul class="fancy-list">
                ');
                while ($row = $query->fetch_assoc()){
                    $id_prod = $row["id_prod"];
                    $query_prod = $conn ->query("SELECT * FROM prodotto WHERE id = $id_prod");

                    if($query_prod ->num_rows){
                        while($row2 = $query_prod->fetch_assoc()){
                            $img = $row2["immagine"];
                            $nome = $row2['nome'];
                            $qnt = $row['quantita'];
                            $prezzo = $row2['prezzo'] * $qnt;
                            echo("<li class=\"flexbox align-top\">
                            <div class=\"image\">
                                <img src=\"images/$img.jpg\" alt=\"$nome\">
                            </div>
                            <div>
                                <h2>$nome</h2>
                                <p>
                                    Quantità: $qnt<br>
                                    Prezzo: $prezzo €<br>
                                </p>
                                <form method=\"post\" action=\"\">
                                    <button type=\"submit\" name=\"removeProd\"> Rimuovi dal carrello </button>
                                    <input type=\"hidden\" value=\"$id_prod\" name=\"idProd\">
                                </form>
                            </div>
                        </li>");
                        }
                    }
                }
                echo('</ul>');
              }
        ?>
    </div>
</body>
<?php include './footer.php'; ?>
<style>
    @media screen and (max-width: 850px){
        #f{ display: block; }
        #main .prod-image{ width: 80vw; margin: 0 auto; }
        #padded{ padding: 40px; }
    }
</style>
<?php include './closeConn.php'?>
</html>

<script>
    let dropdown_parent = document.getElementsByClassName('dropdown');
    Array.from(dropdown_parent)
        .forEach(item => {

            let children = Array.from(item.children);
            let event = Array.from(children[0].children);
            console.log(event[1]);

            event[1].addEventListener('click', (e) => {

                let icon = Array.from(children[0].children)[1]

                if (children[1].classList.contains('dropped')) {

                    icon.style.transform = 'rotate(0)'
                }
                else {
                    icon.style.transform = 'rotate(180deg)'
                }

                for(var i=1; i<children.length; i++){
                    if (children[i].classList.contains('dropped')) {
                        children[i].style.display = 'none';
                        children[i].classList.remove('dropped');
                    }else{
                        children[i].style.display = 'block';
                        children[i].classList.add('dropped');
                    }
                }

            })

        })

</script>

<script src="./actions.js"></script>

<script>
  $( ".slideOut" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideOut(data_target);
  });

  $( ".slideIn" ).on( "click", function() {
    var data_target = $(this).data("target");
    slideIn(data_target);
  });

  $(".toggle").on("click", function(){
    var data_target = $(this).data("target");
    toggle(data_target);
  });

</script>

<script src="./actions.js"></script>
